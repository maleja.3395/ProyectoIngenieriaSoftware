﻿$(document).ready(function () {
    try {
        $('.autocomplete-with-hidden').autocomplete({
            minLength: 0,
            idPrecio: "",
            idProducto: "",
            idNombre: "",
            idIva: "",
            source: function (request, response) {
                var url = $(this.element).data("url");
                var id = $(this.element)[0].id;
                var numFila = id.substring(20);
                idPrecio = "#precioProducto" + numFila;
                idProducto = "#idProducto" + numFila;
                idNombre = "#autocompleteProducto" + numFila;
                idIva = "#valorIvaProducto" + numFila;
                $.getJSON(url, { termino: request.term }, function (data) {
                    response(data);
                })
                $("body span div").css("display", "none");
            },
            select: function (event, ui) {
                $(idPrecio).val(ui.item.precioTotal);
                $(idProducto).val(ui.item.id);
                $(idIva).val(ui.item.iva);
            },
            change: function (event, ui) {
                if (!ui.item) {
                    $(event.target).val('').next('input[type=hidden]').val('');
                    $("body span div").css("display", "none");
                }
            },
            response: function (event, ui) {
            }
        });
    }catch (e) {

    }

    try {
        if (listarVentas) {
            $.ajax({
                url: urlListarVentas,
                type: 'POST',
                data: {},
                success: function (resultado) {
                    if (resultado == "") {
                    }
                    else {
                        llenarTablaVentas(resultado);
                    }
                },
                error: function (resultado) {
                }
            })
        }
    } catch (e) {

    }

    try {
        if (listarProductos)
        {
            ListarProductos();
            ListarCategorias();
        }
    } catch (e) {

    }

    try {
        $('#autocompleteProveedor').autocomplete({
            minLength: 0,
            source: function (request, response) {
                var url = $(this.element).data("url");
                $.getJSON(url, { termino: request.term }, function (data) {
                    response(data);
                })
                $("body span div").css("display", "none");
            },
            select: function (event, ui) {
                $('#idProveedor').val(ui.item.id);
            },
            change: function (event, ui) {
                if (!ui.item) {
                    $(event.target).val('').next('input[type=hidden]').val('');
                    $("body span div").css("display", "none");
                }
            },
            response: function (event, ui) {
            }
        });
    } catch (e) {

    }
});

function AgregarProductoCompra() {
    var tableRef = document.getElementById("tablaProductosCompra").getElementsByTagName('tbody')[0];
    var newRow = tableRef.insertRow(tableRef.rows.length);
    var numFila = tableRef.rows.length;
    var autocompleteDetalle = '<div class="form-group"> ' +
    '<input  class="autocomplete-with-hidden form-control ui-autocomplete-input" id="autocompleteProducto' + numFila + '" required="required" type="text" value="" data-url="/Productos/AutocompletadorProducto" autocomplete="off" placeholder="Ingrese el nombre del producto." style="max-width:98%">' +
    '<input class="form-control" id="idProducto' + numFila + '" style="display:none;" /></div>';
    var cell0 = newRow.insertCell(0);
    cell0.innerHTML = autocompleteDetalle;
    var cell1 = newRow.insertCell(1);
    cell1.innerHTML = '<input class="form-control input-sm" id="precioProducto' + numFila + '" style="text-align:right;" disabled/>';
    var cell2 = newRow.insertCell(2);
    cell2.innerHTML = '<input class="form-control input-sm" id="valorIvaProducto' + numFila + '" style="text-align:right;" disabled/>';
    var cell3 = newRow.insertCell(3);
    cell3.innerHTML = "<input class='form-control input-sm' id='cantidadPrecio" + numFila + "' onkeyup='CalcularTotalProducto();'/>";
    var cell4 = newRow.insertCell(4);
    cell4.innerHTML = "<input class='form-control input-sm' id='valorTotalProducto" + numFila + "' style='text-align:right;' disabled/>";
    var cell5 = newRow.insertCell(5);
    cell5.innerHTML = "<button type='button' onclick='EliminarProducto(this)' id='btnEliminar" + numFila + "' style='background-color:transparent; border-color:transparent;'  title='Elimina el producto seleccionado'> " +
                        " <span class='glyphicon glyphicon-remove'></span></button>";

    InicializarAutocompleteDetalle();
}

function abrirModalFactura() {
    $('#modalLoading').modal('show');
}

function cerrarModalFactura() {
    $('#modalLoading').modal('hide');
}

function InicializarAutocompleteDetalle() {
    $('.autocomplete-with-hidden').autocomplete({
        minLength: 0,
        idPrecio: "",
        idProducto: "",
        idNombre: "",
        idIva: "",
        source: function (request, response) {
            var url = $(this.element).data("url");
            var id = $(this.element)[0].id;
            var numFila = id.substring(20);
            idPrecio = "#precioProducto" + numFila;
            idProducto = "#idProducto" + numFila;
            idNombre = "#autocompleteProducto" + numFila;
            idIva = "#valorIvaProducto" + numFila;
            $.getJSON(url, { termino: request.term }, function (data) {
                response(data);
            })
            $("body span div").css("display", "none");
        },
        select: function (event, ui) {
            $(idPrecio).val(ui.item.precioTotal);
            $(idProducto).val(ui.item.id);
            $(idIva).val(ui.item.iva);
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(event.target).val('').next('input[type=hidden]').val('');
                $("body span div").css("display", "none");
            }
        },
        response: function (event, ui) {
        }
    });
}

function CalcularTotalProducto() {
    var fila, cantidad, valor;
    var valorTotal = 0, totalCompra = 0, iva = 0, totalIva = 0, subTotal = 0;
    var registros = document.getElementById("tablaProductosCompra").tBodies[0].children;
    var cantidadProductos = document.getElementById("tablaProductosCompra").tBodies[0].getElementsByTagName("tr").length;
    for (i = 0; i < cantidadProductos; i++) {
        fila = registros[i];
        cantidad = fila.cells[3].getElementsByTagName("input")[0].value;
        if (cantidad == "" || cantidad == undefined)
        {
            cantidad = "0";
        }
        cantidad = parseInt(cantidad);
        valor = fila.cells[1].getElementsByTagName("input")[0].value;
        valor = valor.replace(/,/g, "");
        valor = parseFloat(valor);
        iva = fila.cells[2].getElementsByTagName("input")[0].value;
        iva = iva.replace(/,/g, "");
        iva = parseFloat(iva);
        totalIva = totalIva + (iva * cantidad);
        subTotal = subTotal + (valor * cantidad);
        valor = valor + iva;
        valorTotal = cantidad * valor;
        fila.cells[4].getElementsByTagName("input")[0].value = formatMonedaUSD(valorTotal);
        totalCompra = totalCompra + valorTotal;
    }
    $('#totalCompra').html("$ " + formatMonedaUSD(totalCompra));
    $('#totalIva').html("$ " + formatMonedaUSD(totalIva));
    $('#subtotal').html("$ " + formatMonedaUSD(subTotal));
}

function formatMonedaUSD(valor) {
    return valor.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}

function EliminarProducto(row) {
    var fila = row.parentNode.parentNode.rowIndex;
    var nombreTabla = row.parentNode.parentNode.parentNode.parentElement.id;

    var rowCount = document.getElementById(nombreTabla).rows.length;

    if (rowCount > 1) {
        document.getElementById(nombreTabla).deleteRow(fila);
        CalcularTotalCompra();
    }
}

function CalcularTotalCompra() {
    var registros = document.getElementById("tablaProductosCompra").tBodies[0].children;
    var tamañoRegistros = document.getElementById("tablaProductosCompra").tBodies[0].getElementsByTagName("tr").length;
    var fila, valorTotal = 0, precio = 0, cantidad = 0, totalCompra = 0, iva = 0, totalIva = 0, subTotal = 0;
    for (i = 0; i < tamañoRegistros; i++) {
        fila = registros[i];
        cantidad = fila.cells[3].getElementsByTagName("input")[0].value;
        if (cantidad == "" || cantidad == undefined) { cantidad = 0; }
        cantidad = parseInt(cantidad);
        precio = fila.cells[1].getElementsByTagName("input")[0].value;
        if (precio == "" || precio == undefined) { precio = "0.00"; }
        precio = precio.replace(/,/g, "");
        precio = parseFloat(precio);
        iva = fila.cells[2].getElementsByTagName("input")[0].value;
        if (iva == "" || iva == undefined) { iva = "0.00"; }
        iva = iva.replace(/,/g, "");
        iva = parseFloat(iva);
        subTotal = subTotal + (precio * cantidad);
        precio = precio + iva;
        valorTotal = cantidad * precio;
        totalCompra = totalCompra + valorTotal;
        totalIva = totalIva + (iva * cantidad);
    }
    $('#totalCompra').html("$ " + formatMonedaUSD(totalCompra));
    $('#totalIva').html("$ " + formatMonedaUSD(totalIva));
    $('#subtotal').html("$ " + formatMonedaUSD(subTotal));
}

function GenerarCompra() {
    abrirModalFactura();

    var listaProductos = new Array();   
    var idComprador = $('#idComprador').val();
    var subTotal = $('#subtotal')[0].innerHTML;
    subTotal = subTotal.substring(2);
    subTotal = subTotal.replace(/,/g, "");
    subTotal = parseFloat(subTotal);
    var iva = $('#totalIva')[0].innerHTML;
    iva = iva.substring(2);
    iva = iva.replace(/,/g, "");
    iva = parseFloat(iva);
    var total = $('#totalCompra')[0].innerHTML;
    total = total.substring(2);
    total = total.replace(/,/g, "");
    total = parseFloat(total);

    listaProductos = ConstruirListaProductos();

    var lista = JSON.stringify({
        listaProductos: listaProductos, idComprador: idComprador,
        subTotal: subTotal, iva: iva, total: total
    });

    $.ajax({
        url: urlRegistrarCompra,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: lista,
        success: function (resultado) {
            if (resultado == "" || resultado == null || resultado == undefined) {
                cerrarModalFactura();
                abrirModalError("Ocurrió un error, no se pudo generar la factura. Pero se registró la compra");
            }
            else {
                cerrarModalFactura();
                //abrirModalError("Ocurrió un error, no se pudo generar la factura. Pero se registró la compra");
                vistaGenerarPDF(resultado);
            }
        },
        error: function (resultado) {
            cerrarModalPdf();
            abrirModalError("Ocurrio un error, asegurese de haber llenado todos los campos");
        }
    });
}

function ConstruirListaProductos() {
    var registrosProducto = document.getElementById("tablaProductosCompra").tBodies[0].children;
    var tamRegistros = registrosProducto.length;
    var detalle, cantidad, idProducto, id, precio, iva, total;
    var registros = new Array();
    var fila;

    for (i = 0; i < tamRegistros; ++i) {
        fila = registrosProducto[i];
        idProducto = "#idProducto" + (i + 1);
        try {
            detalle = fila.cells[0].getElementsByTagName("input")[0].value;
            id = $(idProducto).val();
            precio = fila.cells[1].getElementsByTagName("input")[0].value;
            precio = precio.replace(/,/g, "");
            precio = parseFloat(precio);
            iva = fila.cells[2].getElementsByTagName("input")[0].value;
            iva = iva.replace(/,/g, "");
            iva = parseFloat(iva);
            cantidad = fila.cells[3].getElementsByTagName("input")[0].value;
            total = fila.cells[4].getElementsByTagName("input")[0].value;
            total = total.replace(/,/g, "");
            total = parseFloat(total);
            registros[i] = { "IdProducto": id, "Nombre": detalle, "Precio": precio, "Iva": iva, "PreciTotal": total, "Cantidad": cantidad };
        } catch (e) {
        }
    }

    return registros;
}

function vistaGenerarPDF(resultado) {
    var url = urlCargarGeneradorPDF;
    url = url.replace('valor', resultado);
    window.location.href = url;
}

function llenarTablaVentas(datos) {
    var table = document.getElementById("tablaVentas").getElementsByTagName('tbody')[0];
    var row, cell1, cell2, cell3, cell4, cell5, cell6;

    var subTotal = 0, iva = 0, total = 0;

    for (var i = 0; i < datos.length; i++) {
        row = table.insertRow(i);
        cell1 = row.insertCell(0);
        cell2 = row.insertCell(1);
        cell3 = row.insertCell(2);
        cell4 = row.insertCell(3);
        cell5 = row.insertCell(4);
        cell6 = row.insertCell(5);
        cell1.innerHTML = datos[i].consecutivo;
        cell2.innerHTML = datos[i].idComprador;
        cell3.innerHTML = datos[i].fecha;
        cell4.innerHTML = '$ ' + datos[i].subTotal;
        cell5.innerHTML = '$ ' + datos[i].iva;
        cell6.innerHTML = '$ ' + datos[i].total;

        cell3.style.textAlign = "center";
        cell4.style.textAlign = "right";
        cell5.style.textAlign = "right";
        cell6.style.textAlign = "right";

        subTotal += parseFloat(datos[i].subTotal.replace(/,/g, ""));
        iva += parseFloat(datos[i].iva.replace(/,/g, ""));
        total += parseFloat(datos[i].total.replace(/,/g, ""))
    }

    $('#subTotalCompras').html("$ " + formatMonedaUSD(subTotal));
    $('#ivaCompras').html("$ " + formatMonedaUSD(iva));
    $('#totalCompras').html("$ " + formatMonedaUSD(total));
}

function ListarProductos()
{
    $.ajax({
        url: urlListarProductos,
        type: 'POST',
        data: {},
        success: function (resultado) {
            if (resultado == "") {
            }
            else {
                llenarTablaProductos(resultado);
            }
        },
        error: function (resultado) {
        }
    })
}

function llenarTablaProductos(datos) {
    var table = document.getElementById("tablaProductos").getElementsByTagName('tbody')[0];
    var row, cell1, cell2, cell3, cell4, cell5, cell6, cell7;

    for (var i = 0; i < datos.length; i++) {
        row = table.insertRow(i);
        cell1 = row.insertCell(0);
        cell2 = row.insertCell(1);
        cell3 = row.insertCell(2);
        cell4 = row.insertCell(3);
        cell5 = row.insertCell(4);
        cell6 = row.insertCell(5);
        cell7 = row.insertCell(6);
        cell1.innerHTML = datos[i].nombre;
        cell2.innerHTML = datos[i].proveedor;
        cell3.innerHTML = datos[i].cantidad;
        cell4.innerHTML = datos[i].categoria;
        cell5.innerHTML = '$ ' + datos[i].precioCompra;
        cell6.innerHTML = '$ ' + datos[i].iva;
        cell7.innerHTML = '$ ' + datos[i].precioVenta;

        cell3.style.textAlign = "center";
        cell4.style.textAlign = "center";
        cell5.style.textAlign = "center";
        cell6.style.textAlign = "center";
        cell7.style.textAlign = "center";

    }
}

function ListarCategorias() {
    var selCategorias = $('#selectCategoria');

    $.ajax({
        url: urlListarCategorias,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: { },
        dataType: "json",
        success: function (resultado) {
            $(resultado).each(function (i, v) { // indice, valor
                selCategorias.append('<option value="' + v[0] + '">' + v[1] + '</option>');
            })
        },
        error: function (resultado) {
            abrirModalError("Ocurrio un error");
        }
    })
}

function IngresarProducto() {
    var nombre = $('#nombreProducto').val();
    var idProveedor = $('#idProveedor').val();
    var idCategoria = $('#selectCategoria').val();
    var cantidad = $('#cantidadProducto').val();
    var precioCompra = $('#precioCompra').val();
    var iva = $('#iva').val();
    var precioVenta = $('#precioVenta').val();

    var producto = {
        "Nombre": nombre, "CantidadDisponible": parseFloat(cantidad), "PrecioDeVenta": parseFloat(precioVenta),
        "PrecioSinIva": parseFloat(iva), "PrecioDeCompra": parseFloat(precioCompra), "IdProveedor": idProveedor, "IdCategoria": idCategoria
    }

    $.ajax({
        url: urlIngresarProducto,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(producto),
        success: function (resultado) {
            var tabla = document.getElementById("tablaProductos");
            EliminarFilas(tabla);
            ListarProductos();
            $('#nombreProducto').val("");
            $('#autocompleteProveedor').val("");
            $('#idProveedor').val("");
            $('#cantidadProducto').val("");
            $('#precioCompra').val("");
            $('#iva').val("");
            $('#precioVenta').val("");
            abrirModalInfo("Se realizó la carga exitosamente");
        },
        error: function (resultado) {
            abrirModalError("Ocurrio un error, asegurese de haber llenado todos los campos");
        }
    });

}

function EliminarFilas(tabla) {
    var rows = tabla.rows.length;
    while (rows > 1) {
        tabla.deleteRow(rows - 1);
        rows = tabla.rows.length;
    }
}

function IngresarCategoria() {
    var nombre = $('#nombreCategoria').val();

    $.ajax({
        url: urlIngresarCategoria,
        type: 'POST',
        data: {nombre: nombre},
        success: function (resultado) {
            $('#nombreCategoria').val("");
            abrirModalInfo("Se realizó la carga exitosamente");
        },
        error: function (resultado) {
            abrirModalError("Ocurrio un error, asegurese de haber llenado todos los campos");
        }
    });
}

function IngresarProveedor() {
    var razonSocial = $('#razonSocial').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();

    var Proveedor = { "RazonSocial": razonSocial, "Direccion": direccion, "Telefono": telefono };

    $.ajax({
        url: urlIngresarProveedor,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(Proveedor),
        success: function (resultado) {
            $('#razonSocial').val("");
            $('#direccion').val("");
            $('#telefono').val("");
            abrirModalInfo("Se realizó la carga exitosamente");
        },
        error: function (resultado) {
            abrirModalError("Ocurrio un error, asegurese de haber llenado todos los campos");
        }
    });
}

function abrirModalInfo(mensaje) {
    $("#modalInfoContent").text(mensaje);
    $('#modalInfo').modal('show');
}
function abrirModalError(mensaje) {
    $("#modalErrorContent").text(mensaje);
    $('#modalError').modal('show');
}