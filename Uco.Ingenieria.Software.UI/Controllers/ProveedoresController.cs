﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;

namespace Uco.Ingenieria.Software.UI.Controllers
{
    public class ProveedoresController : Controller
    {
        public IProveedoresRepositorio ProveedoresRepositorio;

        public ProveedoresController(
            IProveedoresRepositorio interfazProveedoresRepositorio)
        {
            this.ProveedoresRepositorio = interfazProveedoresRepositorio;
        }

        public ActionResult Proveedores()
        {
            return View();
        }

        [HttpGet]
        public JsonResult AutocompletadorProveedores(string termino)
        {
            IEnumerable<Proveedor> listaProveedores = this.ProveedoresRepositorio.AutocompleteProveedores(termino);

            return this.ConvertirListaProveedoresJson(listaProveedores);
        }

        private JsonResult ConvertirListaProveedoresJson(IEnumerable<Proveedor> listaProveedores)
        {
            List<object> listaProveedoresJson = new List<object>();
            listaProveedores.ToList().ForEach(p => listaProveedoresJson.Add(new
            {
                id = p.Id,
                label = p.RazonSocial
            }));

            return this.Json(listaProveedoresJson, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CrearProveedor(Proveedor nuevoProveedor)
        {
           nuevoProveedor.Id = Guid.NewGuid();

            var resultado = this.ProveedoresRepositorio.IngresarProveedor(nuevoProveedor);

            return this.Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}