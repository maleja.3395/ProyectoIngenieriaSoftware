﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;
using Uco.Ingenieria.Software.Utilidades;
using Uco.Ingenieria.Software.Utilidades.Modelos;

namespace Uco.Ingenieria.Software.UI.Controllers
{
    public class ComprasController : Controller
    {
        public IComprasRepositorio ComprasRepositorio;

        public IProductosRepositorio ProductosRepositorio;
        public IGeneradorReporte GeneradorReporte;
        public ComprasController(
            IComprasRepositorio interfazComprasRepositorio,
            IProductosRepositorio interfazProductoRepositorio)
        {
            ComprasRepositorio = interfazComprasRepositorio;
            ProductosRepositorio = interfazProductoRepositorio;
        }

        public ActionResult ListarVentas()
        {
            return View();
        }

        [HttpPost]
        public JsonResult RegistrarVenta(
            IEnumerable<ProductosPorVenta> listaProductos, int idComprador, decimal subTotal, 
            decimal iva, decimal total)
        {
            Random random = new Random();
            int consecutivo = random.Next();
            Guid idVenta = Guid.NewGuid();
            this.ComprasRepositorio.RegistrarVenta(idVenta, listaProductos, idComprador, consecutivo, subTotal, iva, total);

            return Json(idVenta, JsonRequestBehavior.AllowGet);
        }

        public FileResult GenerarEnviarPDF(string consecutivo)
        {
            FileResult resultado = null;
            try
            {
                Venta venta = this.ComprasRepositorio.ConsultarVenta(new Guid(consecutivo));
                IEnumerable<ProductosPorVenta> listaProductos = this.ProductosRepositorio.ListarProductosPorVenta(venta.Id);
                ConfiguracionReporte configuracion = new ConfiguracionReporte();

                configuracion.RutaReporte = Path.Combine(Server.MapPath("~/Reportes"), "FacturaVenta.rdlc");
                configuracion.FormatoReporte = FormatosReporte.PDF;

                List<DataSetReporte> dataSets = new List<DataSetReporte>();
                dataSets.Add(new DataSetReporte { Nombre = "VentaDS", Datos = new List<Venta>() { venta } });
                dataSets.Add(new DataSetReporte { Nombre = "ProductosPorVentaDS", Datos = listaProductos });

                configuracion.DataSets = dataSets;

                List<ParametroReporte> parametros = new List<ParametroReporte>();
                parametros.Add(new ParametroReporte { Nombre = "IdCliente", Valor = venta.IdComprador.ToString() });

                configuracion.ParametrosReporte = parametros;

                configuracion.Pagina = new ConfiguracionPagina { Alto = "11in", Ancho = "8.5", MargenSuperior = "0in", MargenIzquierdo = "0.25in", MargenDerecho = "0.25in", MargenInferior = "0in" };

                byte[] renderedBytes;
                string mimeType = "";

                renderedBytes = this.GeneradorReporte.GenerarReporte(configuracion, out mimeType);

                FileResult archivo = File(renderedBytes, mimeType);
                Session["ExportarFactura"] = archivo;

                resultado = archivo;
            }
            catch (Exception ex)
            {
            }

            return resultado;
        }

        [HttpPost]
        public JsonResult ListarTodasLasVentas()
        {
            IEnumerable<Venta> listaVentas = this.ComprasRepositorio.ListaVentas();

            return this.ConvertirVentasJsonRepositorio(listaVentas);
        }

        private JsonResult ConvertirVentasJsonRepositorio(IEnumerable<Venta> listaVentas)
        {
            List<object> listaVentasJson = new List<object>();

            listaVentas.ToList().ForEach(v => listaVentasJson.Add(new
            {
                consecutivo = v.Consecutivo,
                idComprador = v.IdComprador == null ? "Sin Registro" : v.IdComprador.Value.ToString(),
                fecha = v.Fecha.ToString("yyyy/MM/dd", DateTimeFormatInfo.InvariantInfo),
                subTotal = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", v.SubTotalCompra),
                iva = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", v.IvaCompra),
                total = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", v.TotalCompra),
            }));

            return this.Json(listaVentasJson, JsonRequestBehavior.AllowGet);
        }
    }
}