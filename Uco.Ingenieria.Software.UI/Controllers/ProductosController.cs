﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;
using Uco.Ingenieria.Software.Utilidades;

namespace Uco.Ingenieria.Software.UI.Controllers
{
    public class ProductosController : Controller
    {
        public IProductosRepositorio ProductosRepositorio;
        public IProveedoresRepositorio ProveedoresRepositorio;
        public ICategoriasRepositorio CatergoriasRepositorio;

        public ProductosController(
            IProductosRepositorio interfazProductosRepositorio,
            IProveedoresRepositorio interfazProveedoresRepositorio,
            ICategoriasRepositorio interfazCategoriasRepositorio)
        {
            this.ProductosRepositorio = interfazProductosRepositorio;
            this.CatergoriasRepositorio = interfazCategoriasRepositorio;
            this.ProveedoresRepositorio = interfazProveedoresRepositorio;
        }

        public ActionResult RealizarCompra()
        {
            return View();
        }

        public ActionResult IngresarProducto()
        {
            return View();
        }

        [HttpGet]
        public JsonResult AutocompletadorProducto(string termino)
        {
            IEnumerable<Producto> listaProductos = this.ProductosRepositorio.AutocompleteProductos(termino);

            return this.ConvertirListaProductosJson(listaProductos);
        }

        private JsonResult ConvertirListaProductosJson(IEnumerable<Producto> listaProductos)
        {
            List<object> listaProductosJson = new List<object>();
            listaProductos.ToList().ForEach(p => listaProductosJson.Add(new
            {
                id = p.Id,
                label = p.Nombre,
                iva = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", p.PrecioSinIva),
                precioTotal = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", p.PrecioDeVenta)
            }));

            return this.Json(listaProductosJson, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarTodosLosProductos()
        {
            IEnumerable<Producto> listaProductos = this.ProductosRepositorio.ListarProductos();

            return this.ConvertirListaTodosLosProductosJson(listaProductos);
        }

        private JsonResult ConvertirListaTodosLosProductosJson(IEnumerable<Producto> listaProductos)
        {
            List<object> listaProductosJson = new List<object>();
            listaProductos.ToList().ForEach(p => listaProductosJson.Add(new
            {
                id = p.Id,
                nombre = p.Nombre,
                cantidad = p.CantidadDisponible,
                categoria = this.CatergoriasRepositorio.ConsultarCategoria(p.IdCategoria).Nombre,
                proveedor = this.ProveedoresRepositorio.ConsultarProveedor(p.IdProveedor).RazonSocial,
                iva = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", p.PrecioSinIva),
                precioVenta = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", p.PrecioDeVenta),
                precioCompra = string.Format(CultureInfo.InvariantCulture, "{0:" + Constantes.FORMATO_DECIMAL + "}", p.PrecioDeCompra)
            }));

            return this.Json(listaProductosJson, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IngresarProducto(Producto producto)
        {
            producto.Id = Guid.NewGuid();

            var resultado = this.ProductosRepositorio.AgregarProducto(producto);

            return this.Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}