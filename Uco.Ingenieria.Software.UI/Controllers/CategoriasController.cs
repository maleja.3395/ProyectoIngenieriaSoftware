﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;

namespace Uco.Ingenieria.Software.UI.Controllers
{
    public class CategoriasController : Controller
    {
        public ICategoriasRepositorio CatergoriasRepositorio;
        public CategoriasController(
            ICategoriasRepositorio interfazCategoriasRepositorio)
        {
            this.CatergoriasRepositorio = interfazCategoriasRepositorio;
        }

        [HttpGet]
        public JsonResult ListarCategorias()
        {
            IEnumerable<Categoria> listaCategorias = this.CatergoriasRepositorio.ListaCategorias();
            List<object> listaCategoriasJson = new List<object>();

            listaCategorias.ToList().ForEach(c => listaCategoriasJson.Add(new
                 string[]{
                    c.Id.ToString(),
                    c.Nombre,
                    }));

            return this.Json(listaCategoriasJson, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CrearCategoria(string nombre)
        {
            Categoria nuevaCategoria = new Categoria { Nombre = nombre, Id = Guid.NewGuid() };

            var resultado = this.CatergoriasRepositorio.CrearCategoria(nuevaCategoria);

            return this.Json(resultado, JsonRequestBehavior.AllowGet);
        }
    }
}