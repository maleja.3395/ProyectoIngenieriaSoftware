﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Uco.Ingenieria.Software.UI.Startup))]
namespace Uco.Ingenieria.Software.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
