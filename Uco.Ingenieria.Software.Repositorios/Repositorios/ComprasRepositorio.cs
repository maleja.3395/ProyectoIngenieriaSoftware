﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;

namespace Uco.Ingenieria.Software.Repositorios.Repositorios
{
    public class ComprasRepositorio : IComprasRepositorio
    {
        readonly EntidadesProyecto contextoEntidades;

        public ComprasRepositorio()
        {
            contextoEntidades = new EntidadesProyecto();
        }

        public bool RegistrarVenta(Guid idVenta, IEnumerable<ProductosPorVenta> listaProductos, int idComprador, int consecutivo, decimal subTotal, decimal iva, decimal total)
        {
            Venta nuevaVenta = new Venta()
            {
                Id = idVenta,
                Consecutivo = consecutivo,
                IdComprador = idComprador,
                Fecha = DateTime.Now,
                SubTotalCompra = subTotal,
                IvaCompra = iva,
                TotalCompra = total
            };

            contextoEntidades.Venta.Add(nuevaVenta);
            bool registrarProductos = this.RegistrarProductoVenta(nuevaVenta.Id, listaProductos);
            return contextoEntidades.SaveChanges() == 1;
        }

        private bool RegistrarProductoVenta(Guid id, IEnumerable<ProductosPorVenta> listaProductos)
        {
            listaProductos.ToList().ForEach(p => {
                p.IdProductoVenta = Guid.NewGuid();
                p.IdVenta = id;
                contextoEntidades.ProductosPorVenta.Add(p);

                var producto = (from productos in contextoEntidades.Producto
                            where productos.Id == p.IdProducto
                            select productos).SingleOrDefault();

                producto.CantidadDisponible = producto.CantidadDisponible - p.Cantidad;
                }
            );

            return contextoEntidades.SaveChanges() == 1;
        }

        public Venta ConsultarVenta(Guid idVenta)
        {
            return contextoEntidades.Venta.Where(v => v.Id == idVenta).FirstOrDefault();
        }

        public IEnumerable<Venta> ListaVentas()
        {
            return contextoEntidades.Venta.ToList();
        }
    }
}
