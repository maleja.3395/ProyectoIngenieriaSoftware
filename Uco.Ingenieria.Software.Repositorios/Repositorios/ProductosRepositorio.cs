﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;

namespace Uco.Ingenieria.Software.Repositorios.Repositorios
{
    public class ProductosRepositorio : IProductosRepositorio
    {
        readonly EntidadesProyecto contextoEntidades;

        public ProductosRepositorio()
        {
            contextoEntidades = new EntidadesProyecto();
        }

        public IEnumerable<Producto> AutocompleteProductos(string termino)
        {
            IEnumerable<Producto> listaProductos;

            if (termino.Length < 4)
            {
                listaProductos = new List<Producto>();
            }
            else
            {
                listaProductos = ListarProductosPorTermino(termino);
            }

            return listaProductos;
        }

        private IEnumerable<Producto> ListarProductosPorTermino(string termino)
        {
            var respuesta = contextoEntidades.Producto.
                Where(p => p.Nombre.Contains(termino)).OrderBy(p => p.Nombre);

            return respuesta.ToList();
        }

        public Producto ConsultarProducto(string codigo)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductosPorVenta> ListarProductosPorVenta(Guid idVenta)
        {
            return contextoEntidades.ProductosPorVenta.Where(p => p.IdVenta == idVenta).ToList().OrderBy(p => p.Nombre);
        }

        public bool AgregarProducto(Producto producto)
        {
            contextoEntidades.Producto.Add(producto);
            return contextoEntidades.SaveChanges() == 1;
        }

        public IEnumerable<Producto> ListarProductos()
        {
            return contextoEntidades.Producto.ToList().OrderBy(p => p.Nombre);
        }
    }
}
