﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;

namespace Uco.Ingenieria.Software.Repositorios.Repositorios
{
    public class CategoriasRepositorio : ICategoriasRepositorio
    {
        readonly EntidadesProyecto contextoEntidades;

        public CategoriasRepositorio()
        {
            contextoEntidades = new EntidadesProyecto();
        }

        public Categoria ConsultarCategoria(Guid idCategoria)
        {
            return contextoEntidades.Categoria.Where(c => c.Id == idCategoria).FirstOrDefault();
        }

        public bool CrearCategoria(Categoria categoria)
        {
            contextoEntidades.Categoria.Add(categoria);

            return contextoEntidades.SaveChanges() == 1;
        }

        public IEnumerable<Categoria> ListaCategorias()
        {
            var respuesta = contextoEntidades.Categoria.OrderBy(p => p.Nombre);

            return respuesta.ToList();
        }
    }
}
