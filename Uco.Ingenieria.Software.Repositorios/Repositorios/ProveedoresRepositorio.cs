﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uco.Ingeniera.Software.Entidades;
using Uco.Ingeniera.Software.Entidades.Contratos;

namespace Uco.Ingenieria.Software.Repositorios.Repositorios
{
    public class ProveedoresRepositorio : IProveedoresRepositorio
    {
        readonly EntidadesProyecto contextoEntidades;

        public ProveedoresRepositorio()
        {
            contextoEntidades = new EntidadesProyecto();
        }
        public IEnumerable<Proveedor> AutocompleteProveedores(string termino)
        {
            IEnumerable<Proveedor> listaProductos;

            if (termino.Length < 4)
            {
                listaProductos = new List<Proveedor>();
            }
            else
            {
                listaProductos = ListarProveedoresPorTermino(termino);
            }

            return listaProductos;
        }

        public Proveedor ConsultarProveedor(Guid idProveedor)
        {
            return contextoEntidades.Proveedor.Where(p => p.Id == idProveedor).FirstOrDefault();
        }

        public bool IngresarProveedor(Proveedor proveedor)
        {
            contextoEntidades.Proveedor.Add(proveedor);
            return contextoEntidades.SaveChanges() == 1;
        }

        private IEnumerable<Proveedor> ListarProveedoresPorTermino(string termino)
        {
            var respuesta = contextoEntidades.Proveedor.
                Where(p => p.RazonSocial.Contains(termino)).OrderBy(p => p.RazonSocial);

            return respuesta.ToList();
        }
    }
}
