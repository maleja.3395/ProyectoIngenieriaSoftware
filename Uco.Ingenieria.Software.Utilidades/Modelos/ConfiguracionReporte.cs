﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingenieria.Software.Utilidades.Modelos
{
    public class ConfiguracionReporte
    {
        public FormatosReporte FormatoReporte { get; set; }
        public string RutaReporte { get; set; }
        public IEnumerable<DataSetReporte> DataSets { get; set; }
        public IEnumerable<ParametroReporte> ParametrosReporte { get; set; }
        public ConfiguracionPagina Pagina { get; set; }
    }
}
