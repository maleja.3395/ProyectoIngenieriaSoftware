﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingenieria.Software.Utilidades.Modelos
{
    public class ParametroReporte
    {
        public ParametroReporte()
        {
            Visible = true;
        }
        public string Nombre { get; set; }
        public string Valor { get; set; }
        public string[] Valores { get; set; }
        public bool Visible { get; set; }
    }
}
