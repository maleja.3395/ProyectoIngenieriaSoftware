﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingenieria.Software.Utilidades.Modelos
{
    public class GeneradorReportesReportViewer : IGeneradorReporte
    {
        public byte[] GenerarReporte(ConfiguracionReporte configuracion, out string mimeType)
        {
            LocalReport reporte = new LocalReport();
            reporte.EnableHyperlinks = true;
            reporte.ReportPath = configuracion.RutaReporte;
            string reportType = configuracion.FormatoReporte.ToString();
            string encoding;
            string fileNameExtension;

            this.ConfigurarDataSets(configuracion.DataSets, reporte);

            this.ConfigurarParametros(configuracion.ParametrosReporte, reporte);

            string deviceInfo = this.GenerarDeviceInfo(reportType, configuracion.Pagina);

            Warning[] wanings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = reporte.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out wanings);

            return renderedBytes;
        }

        private void ConfigurarParametros(IEnumerable<ParametroReporte> parametrosReporte, LocalReport reporte)
        {
            parametrosReporte.ToList().ForEach(param =>
            {
                reporte.SetParameters(new ReportParameter(param.Nombre, param.Valor));
            });
        }

        private void ConfigurarDataSets(IEnumerable<DataSetReporte> dataSets, LocalReport reporte)
        {
            dataSets.ToList().ForEach(ds =>
            {
                reporte.DataSources.Add(new ReportDataSource(ds.Nombre, ds.Datos));
            });
        }

        private string GenerarDeviceInfo(string formatoSalida, ConfiguracionPagina pagina)
        {
            return "<DeviceInfo>" +
                "<OutPutFormat>" + formatoSalida + "</OutPutFormat>" +
                "<PageWidth>" + pagina.Ancho + "</PageWidth>" +
                "<PageHeight>" + pagina.Alto + "</PageHeight>" +
                "<MarginTop>" + pagina.MargenSuperior + "</MarginTop>" +
                "<MarginLeft>" + pagina.MargenIzquierdo + "</MarginLeft>" +
                "<MarginRight>" + pagina.MargenDerecho + "</MarginRight>" +
                "<MarginBottom>" + pagina.MargenInferior + "</MarginBottom>" +
                "</DeviceInfo>";
        }
    }
}
