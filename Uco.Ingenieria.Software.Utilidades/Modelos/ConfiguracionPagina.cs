﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingenieria.Software.Utilidades.Modelos
{
    public class ConfiguracionPagina
    {
        public string Alto { get; set; }
        public string Ancho { get; set; }
        public string MargenDerecho { get; set; }
        public string MargenSuperior { get; set; }
        public string MargenIzquierdo { get; set; }
        public string MargenInferior { get; set; }
    }
}
