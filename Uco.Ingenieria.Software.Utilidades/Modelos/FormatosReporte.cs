﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingenieria.Software.Utilidades.Modelos
{
    public enum FormatosReporte
    {
        PDF,
        Excel,
        Word,
        Image
    }
}
