﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingenieria.Software.Utilidades.Modelos
{
    public class DataSetReporte
    {
        public string Nombre { get; set; }
        public IEnumerable Datos { get; set; }
        public IEnumerable<DataSetReporte> DataSets { get; set; }

    }
}
