﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingeniera.Software.Entidades.Contratos
{
    public interface IProductosRepositorio
    {
        Producto ConsultarProducto(string codigo);
        IEnumerable<Producto> AutocompleteProductos(string termino);
        IEnumerable<ProductosPorVenta> ListarProductosPorVenta(Guid idVenta);
        bool AgregarProducto(Producto producto);
        IEnumerable<Producto> ListarProductos();
    }
}
