﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingeniera.Software.Entidades.Contratos
{
    public interface ICategoriasRepositorio
    {
        IEnumerable<Categoria> ListaCategorias();
        Categoria ConsultarCategoria(Guid idCategoria);
        bool CrearCategoria(Categoria categoria);
    }
}
