﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingeniera.Software.Entidades.Contratos
{
    public interface IComprasRepositorio
    {
        bool RegistrarVenta(Guid idVenta, IEnumerable<ProductosPorVenta> listaProductos, int idComprador,int consecutivo, decimal subTotal, decimal iva, decimal total);
        Venta ConsultarVenta(Guid idVenta);
        IEnumerable<Venta> ListaVentas();
    }
}
