﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uco.Ingeniera.Software.Entidades.Contratos
{
    public interface IProveedoresRepositorio
    {
        IEnumerable<Proveedor> AutocompleteProveedores(string termino);
        Proveedor ConsultarProveedor(Guid idProveedor);
        bool IngresarProveedor(Proveedor proveedor);
    }
}
