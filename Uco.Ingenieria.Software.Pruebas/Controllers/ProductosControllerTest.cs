﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uco.Ingeniera.Software.Entidades.Contratos;
using Uco.Ingenieria.Software.UI.Controllers;
using Uco.Ingeniera.Software.Entidades;
using System.Web.Mvc;

namespace Uco.Ingenieria.Software.Pruebas.Controllers
{
    [TestFixture]
    public class ProductosControllerTest
    {
        private ProductosController ProductosController;
        private Mock<IProveedoresRepositorio> ProveedoresRepositorioMock;
        private Mock<ICategoriasRepositorio> CategoriasRepositorioMock;
        private Mock<IProductosRepositorio> ProductosRepositorioMock;

        [SetUp]
        public void SetUp()
        {
            ProductosRepositorioMock = new Mock<IProductosRepositorio>();
            CategoriasRepositorioMock = new Mock<ICategoriasRepositorio>();
            ProveedoresRepositorioMock = new Mock<IProveedoresRepositorio>();

            ProductosRepositorioMock.Setup(p => p.AutocompleteProductos(It.IsAny<string>())).Returns(this.ListarProductos());
            ProductosRepositorioMock.Setup(p => p.ListarProductos()).Returns(this.ListarProductos());
            ProductosRepositorioMock.Setup(p => p.AgregarProducto(It.IsAny<Producto>())).Returns(true);

            ProveedoresRepositorioMock.Setup(p => p.ConsultarProveedor(It.IsAny<Guid>())).Returns(new Proveedor { RazonSocial = "proveedor" });

            CategoriasRepositorioMock.Setup(c => c.ConsultarCategoria(It.IsAny<Guid>())).Returns(new Categoria { Nombre = "categoria" });

            ProductosController = new ProductosController(ProductosRepositorioMock.Object, ProveedoresRepositorioMock.Object, CategoriasRepositorioMock.Object);

        }

        [TearDown]
        public void TearDown()
        {
            ProductosController = null;
        }

        [Test]
        public void RealizarCompraTest()
        {
            //arrange

            //act
            var resultado = this.ProductosController.RealizarCompra();

            //assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(typeof(ViewResult), resultado.GetType());
        }

        [Test]
        public void IngresarProductoTest()
        {
            //arrange

            //act
            var resultado = this.ProductosController.IngresarProducto();

            //assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(typeof(ViewResult), resultado.GetType());
        }

        [Test]
        public void AutocompletadorProductoTest()
        {
            //arrange
            string termino = "producto";

            //act
            var resultado = this.ProductosController.AutocompletadorProducto(termino);

            //assert
            Assert.IsNotNull(resultado);
            Assert.IsNotNull(resultado.Data);
            Assert.AreEqual(1, (resultado.Data as List<object>).Count);
        }

        [Test]
        public void ListarTodosLosProductosTest()
        {
            //arrange

            //act
            var resultado = this.ProductosController.ListarTodosLosProductos();

            //assert
            Assert.IsNotNull(resultado);
            Assert.IsNotNull(resultado.Data);
            Assert.AreEqual(1, (resultado.Data as List<object>).Count);
        }

        [Test]
        public void IngresarNuevaProductoTest()
        {
            //arrange
            Producto producto = new Producto
            {
                CantidadDisponible = 12,
                IdCategoria = Guid.NewGuid(),
                IdProveedor = Guid.NewGuid(),
                Nombre = "Producto",
                PrecioDeCompra = 1000,
                PrecioDeVenta = 2000,
                PrecioSinIva = 200
            };

            //act
            var resultado = this.ProductosController.IngresarProducto(producto);

            //assert
            Assert.IsNotNull(resultado);
            Assert.IsNotNull(resultado.Data);
            Assert.AreEqual(true, (resultado.Data as object));
        }

        private IEnumerable<Producto> ListarProductos()
        {
            List<Producto> listaProductos = new List<Producto>();
            Producto producto = new Producto {
                Id = Guid.NewGuid(),
                Nombre = "producto",
                PrecioSinIva = 100,
                PrecioDeVenta = 2000,
                CantidadDisponible = 10,
                IdCategoria = Guid.NewGuid(),
                IdProveedor = Guid.NewGuid(),
                PrecioDeCompra = 800
            };

            listaProductos.Add(producto);

            return listaProductos;
        }
    }
}
